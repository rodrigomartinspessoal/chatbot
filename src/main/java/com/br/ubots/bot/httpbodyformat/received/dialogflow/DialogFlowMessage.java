package com.br.ubots.bot.httpbodyformat.received.dialogflow;

public class DialogFlowMessage {

    private Result result;

    private Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getDate() {
        return this.getResult().getParameters().getDate();
    }

    public String getCityName() {
        return this.getResult().getParameters().getGeoCity();
    }

    public String getResolvedQuery() {
        return this.getResult().getResolvedQuery();
    }

    public String getMetadata() {
        return this.getResult().getMetadata().getIntentName();
    }

}
