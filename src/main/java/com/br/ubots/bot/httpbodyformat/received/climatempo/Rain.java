package com.br.ubots.bot.httpbodyformat.received.climatempo;

public class Rain {

    private int precipitation;
    private int probability;

    public int getPrecipitation() {
        return precipitation;
    }

    public void setPrecipitation(int precipitation) {
        this.precipitation = precipitation;
    }

    public int getProbability() {
        return probability;
    }

    public void setProbability(int probability) {
        this.probability = probability;
    }
}
