package com.br.ubots.bot.httpbodyformat.received.climatempo;

public class Data {

    private Wind wind;
    private Humidity humidity;
    private String date;
    private TextIcon text_icon;
    private ThermalSensation thermal_sensation;
    private Rain rain;
    private Temperature temperature;

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Humidity getHumidity() {
        return humidity;
    }

    public void setHumidity(Humidity humidity) {
        this.humidity = humidity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public TextIcon getText_icon() {
        return text_icon;
    }

    public void setText_icon(TextIcon text_icon) {
        this.text_icon = text_icon;
    }

    public ThermalSensation getThermal_sensation() {
        return thermal_sensation;
    }

    public void setThermal_sensation(ThermalSensation thermal_sensation) {
        this.thermal_sensation = thermal_sensation;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }
}
