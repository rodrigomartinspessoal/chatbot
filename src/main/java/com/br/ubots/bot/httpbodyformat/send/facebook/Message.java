package com.br.ubots.bot.httpbodyformat.send.facebook;

public class Message {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
