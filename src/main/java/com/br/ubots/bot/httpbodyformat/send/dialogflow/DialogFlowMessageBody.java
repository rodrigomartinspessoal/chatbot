package com.br.ubots.bot.httpbodyformat.send.dialogflow;

public class DialogFlowMessageBody {

    private String lang;
    private String query;
    private String sessionId;

    public DialogFlowMessageBody(String lang, String query, String sessionId) {
        this.lang = lang;
        this.query = query;
        this.sessionId = sessionId;
    }

    public String getLang() {
        return lang;
    }

    public String getQuery() {
        return query;
    }

    public String getSessionId() {
        return sessionId;
    }
}
