package com.br.ubots.bot.httpbodyformat.received.climatempo;

public class Wind {

    private int velocity_min;
    private int velocity_max;

    public int getMedia() {
        return (velocity_max + velocity_min)/2;
    }

    public void setVelocity_min(int velocity_min) {
        this.velocity_min = velocity_min;
    }

    public void setVelocity_max(int velocity_max) {
        this.velocity_max = velocity_max;
    }
}
