package com.br.ubots.bot.httpbodyformat.received.facebook;

public class FacebookReceivedMessageInfo {

    public static String getText(FacebookMessageBody message) {
        return message.getEntry()[0].getMessaging()[0].getMessage().getText();
    }

    public static String getSenderId(FacebookMessageBody message) {
        return message.getEntry()[0].getMessaging()[0].getSender().getId();
    }

}
