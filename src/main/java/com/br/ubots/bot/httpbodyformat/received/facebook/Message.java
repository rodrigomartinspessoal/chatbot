package com.br.ubots.bot.httpbodyformat.received.facebook;

public class Message {

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private String text;

}
