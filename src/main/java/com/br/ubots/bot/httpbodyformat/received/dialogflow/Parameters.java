package com.br.ubots.bot.httpbodyformat.received.dialogflow;

public class Parameters {

    private String date;
    private String geoCity;

    public String getGeoCity() {
        return geoCity;
    }
    public void setGeoCity(String geoCity) {
        this.geoCity = geoCity;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

}
