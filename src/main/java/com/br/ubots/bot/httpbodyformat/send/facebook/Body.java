package com.br.ubots.bot.httpbodyformat.send.facebook;

public class Body {

    private Message message;
    private String messagingType;
    private Recipient recipient;

    public Body(Message message, String messagingType, Recipient recipient) {
        this.message = message;
        this.messagingType = messagingType;
        this.recipient = recipient;
    }

    public Message getMessage() {
        return message;
    }

    public String getMessagingType() {
        return messagingType;
    }

    public Recipient getRecipient() {
        return recipient;
    }
}
