package com.br.ubots.bot.httpbodyformat.received.facebook;

public class Messaging {

    private Message message;
    private Sender sender;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }
}