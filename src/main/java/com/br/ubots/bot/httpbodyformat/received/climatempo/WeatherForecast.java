package com.br.ubots.bot.httpbodyformat.received.climatempo;

public class WeatherForecast {

    private Data[] data;

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }
}
