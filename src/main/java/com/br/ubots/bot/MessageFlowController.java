package com.br.ubots.bot;

import com.br.ubots.bot.httpbodyformat.received.facebook.FacebookMessageBody;
import com.br.ubots.bot.httpbodyformat.received.facebook.FacebookReceivedMessageInfo;
import com.br.ubots.bot.response.send.*;
import com.br.ubots.bot.response.send.API.FacebookAPI;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

@RestController
public class MessageFlowController {

    private ResponseService responseService;
    private FacebookMessageBuilder facebookMessageBuilder;
    private FacebookAPI facebookAPI;
    private Logger logger;

    public MessageFlowController(ResponseService responseService, FacebookMessageBuilder facebookMessageBuilder, FacebookAPI facebookAPI, Logger logger) {
        this.responseService = responseService;
        this.facebookMessageBuilder = facebookMessageBuilder;
        this.facebookAPI = facebookAPI;
        this.logger = logger;
    }

    @PostMapping("/webhook")
    public ResponseEntity message (@RequestBody FacebookMessageBody message) {
        try {
            String senderId = FacebookReceivedMessageInfo.getSenderId(message);
            String botResponse = getResponse(message, senderId);
            String botResponseInFacebookFormat = facebookMessageBuilder.formatToSend(botResponse, senderId);
            facebookAPI.send(botResponseInFacebookFormat);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            logger.error("["+e.getMessage()+"] ao mandar texto de resposta para o messenger");
        }
        return ResponseEntity.ok().build();
    }

    private String getResponse(FacebookMessageBody message, String senderId) {
        String receivedMessage = FacebookReceivedMessageInfo.getText(message);
        return responseService.getBotResponseMessage(receivedMessage, senderId);
    }
}