package com.br.ubots.bot.config;

import com.br.ubots.bot.response.send.API.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class APIBeanConfig {

    private RestTemplate restTemplate;

    public APIBeanConfig(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Bean
    public ClimatempoAPI climatempoAPI(@Value("${url.climatempo}") String url, @Value("${token.climatempo}") String token) {
        return new ClimatempoAPI(restTemplate, url, token);
    }

    @Bean
    public FacebookAPI facebookAPI(@Value("${url.facebook}") String url) {
        return new FacebookAPI(restTemplate, url);
    }

    @Bean
    public DialogflowAPI dialogflowAPI(@Value("${url.dialogflow}") String url, @Value("${token.dialogflow-auth}") String token) {
        return new DialogflowAPI(restTemplate, url, token);
    }
}
