package com.br.ubots.bot.response.send;

import org.springframework.stereotype.Component;

@Component
public class Email {

    public String getEmail(String[] messages) {
        for (String message : messages) {
            if (message.contains("@")) {
                return message;
            }
        }
        return "";
    }
}
