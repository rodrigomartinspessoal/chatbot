package com.br.ubots.bot.response.send;

import com.br.ubots.bot.httpbodyformat.received.dialogflow.DialogFlowMessage;
import com.br.ubots.bot.httpbodyformat.send.dialogflow.DialogFlowMessageBody;
import com.br.ubots.bot.response.send.API.DialogflowAPI;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import com.google.gson.Gson;
import org.slf4j.Logger;

@Component
public class IntentionService {

    private Gson gson;
    private Logger logger;
    private DialogflowAPI dialogflowAPI;

    public IntentionService(Gson gson, Logger logger, DialogflowAPI dialogflowAPI){
        this.gson = gson;
        this.logger = logger;
        this.dialogflowAPI = dialogflowAPI;
    }

    public DialogFlowMessage getDialogFlowMessageBody(String message, String id) {
        try {
            DialogFlowMessageBody messageInSendDialogflowFormat = new DialogFlowMessageBody("pt-BR", message, id);
            String messageBodyString = gson.toJson(messageInSendDialogflowFormat);
            return dialogflowAPI.getReceivedMessage(messageBodyString);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            logger.error("["+e.getMessage()+"] ao mandar texto de resposta para o messenger");
            throw e;
        }
    }
}