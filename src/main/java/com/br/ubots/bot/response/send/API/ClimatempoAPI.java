package com.br.ubots.bot.response.send.API;

import com.br.ubots.bot.httpbodyformat.received.climatempo.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class ClimatempoAPI {

    private RestTemplate restTemplate;
    private String url;
    private String token;

    public ClimatempoAPI(RestTemplate restTemplate, String url, String token) {
        this.restTemplate = restTemplate;
        this.url = url;
        this.token = token;
    }

    public ResponseEntity<CityInformations[]> cityIdRequest(String city) {
        URI uri = URI.create(url +"locale/city?name="+ city +"&"+ token);
        return restTemplate.getForEntity(uri, CityInformations[].class);
    }

    public Data[] getWeatherForecast(int id) {
        URI uri = URI.create(url +"forecast/locale/"+ id +"/days/15?"+ token);
        ResponseEntity<WeatherForecast> weatherForecast = restTemplate.getForEntity(uri, WeatherForecast.class);
        return weatherForecast.getBody().getData();
    }
}
