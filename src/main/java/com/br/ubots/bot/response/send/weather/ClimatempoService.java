package com.br.ubots.bot.response.send.weather;

import com.br.ubots.bot.httpbodyformat.received.climatempo.CityInformations;
import com.br.ubots.bot.httpbodyformat.received.climatempo.Data;
import com.br.ubots.bot.response.send.API.ClimatempoAPI;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ClimatempoService {

    private ClimatempoAPI climatempoAPI;
    private ForecastMessage forecastMessage;
    private Logger logger;

    ClimatempoService(ClimatempoAPI climatempoAPI, ForecastMessage forecastMessage, Logger logger) {
        this.climatempoAPI = climatempoAPI;
        this.forecastMessage = forecastMessage;
        this.logger = logger;
    }

    public String getWeaterForecast(String city, String date) {
        try {
            return getForecastResponseMessage(city, date);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return "Tive problemas ao buscar a previsão do tempo, verifique se:\n" +
                    "1º A data informada está correta e se é uma data futura.\n" +
                    "2º O nome da cidade está correto e acentuado.";
        }
    }

    private String getForecastResponseMessage(String city, String date) {
        if (sentenceParametersEmpty(city, date)) {
            return responseToParametesEmpty(city);
        } else if (!sentenceParametersEmpty(city, date)){
            CityInformations[] climaTempoRequestResponse = climatempoAPI.cityIdRequest(city).getBody();
            int cityId = getCityId(climaTempoRequestResponse);
            Data[] weatherForecasts = climatempoAPI.getWeatherForecast(cityId);
            return forecastMessage.getWeatherForecastMessage(date, weatherForecasts);
        } else {
            return "";
        }
    }

    public int getCityId(CityInformations[] cityId) {
        if (cityId.length == 0){
            throw new IllegalArgumentException("Erro ao tentar pegar ID referente ao nome da cidade");
        } else {
            return cityId[0].getId();
        }
    }

    private String responseToParametesEmpty(String dialogFlowCity) {
        return (dialogFlowCity.isEmpty())
                ? "Em qual cidade?"
                :"Quando?";
    }

    private boolean sentenceParametersEmpty(String city, String date) {
        return city.isEmpty() || date.isEmpty();
    }
}