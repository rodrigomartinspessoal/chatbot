package com.br.ubots.bot.response.send.weather;

import com.br.ubots.bot.httpbodyformat.received.climatempo.CityInformations;
import com.br.ubots.bot.httpbodyformat.received.climatempo.Data;
import org.springframework.stereotype.Component;

@Component
class ForecastMessage {

    public int getCityId(CityInformations[] cityId) {
        if (cityId.length == 0){
            throw new IllegalArgumentException("Erro ao tentar pegar ID referente ao nome da cidade");
        } else {
            return cityId[0].getId();
        }
    }

    public String getWeatherForecastMessage(String dialogFlowDate, Data[] weatherForecasts) {
        String message = "Não foi possivel encontrar a previsão do tempo para a data informada.";
        for (Data data : weatherForecasts) {
            if (dialogFlowDate.equals(data.getDate())) {
                message = buildForecastMessage(data);
            }
        }
        return message;
    }

    private String buildForecastMessage(Data forecast){
        return ( forecast.getText_icon().getText().getPt()+"!\n" +
                "Temperatura: "+forecast.getTemperature().getMedia()+"ºC\n" +
                "Sensação térmica: "+forecast.getThermal_sensation().getMedia()+"ºC\n" +
                "Vel. Média do vento: "+forecast.getWind().getMedia()+"KM/H\n" +
                "Umidade: "+forecast.getHumidity().getMedia()+"%\n" +
                "Probabilidade de chuva: "+forecast.getRain().getProbability()+"%");
    }
}