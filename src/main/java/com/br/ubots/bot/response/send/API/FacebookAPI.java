package com.br.ubots.bot.response.send.API;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException;
import java.net.URI;

public class FacebookAPI {

    private RestTemplate restTemplate;
    private String url;

    public FacebookAPI(RestTemplate restTemplate, String url) {
        this.restTemplate = restTemplate;
        this.url = url;
    }

    public void send(String messageToSend) throws HttpClientErrorException {
        URI uri = URI.create(url);
        HttpHeaders header = RestAttributes.buildHttpHeaders();
        HttpEntity<String> body = RestAttributes.buildHttpEntity(messageToSend, header);
        restTemplate.postForEntity(uri, body, String.class);
    }
}