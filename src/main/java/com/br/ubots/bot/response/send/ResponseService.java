package com.br.ubots.bot.response.send;

import com.br.ubots.bot.httpbodyformat.received.dialogflow.DialogFlowMessage;
import com.br.ubots.bot.response.send.weather.ClimatempoService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.stringtemplate.v4.*;
import java.util.HashMap;
import java.util.Map;

@Service
public class ResponseService {

    private Map<String, ST> responseMap;
    private IntentionService intentionService;
    private ClimatempoService climatempoService;
    private Email email;

    public ResponseService(IntentionService intentionService, ClimatempoService climatempoService, Email email) {
        this.intentionService = intentionService;
        this.climatempoService = climatempoService;
        this.email = email;
        this.responseMap = buildResponseMap();
    }

    public String getBotResponseMessage(String receivedMessage, String senderId) {
        try {
            DialogFlowMessage dialogFlowMessage = intentionService.getDialogFlowMessageBody(receivedMessage, senderId);
            return getResponse(dialogFlowMessage);
        }catch (HttpClientErrorException e) {
            return responseMap.get("fallback").render();
        }
    }

    private String getResponse(DialogFlowMessage dialogFlowMessage) {
        String intent = dialogFlowMessage.getMetadata();
        ST response = responseMap.get(intent);
        response = cleanStringTemplateValue(response);
        return setStringTemplateValue(response, dialogFlowMessage).render();
    }

    private ST setStringTemplateValue(ST response, DialogFlowMessage dialogFlowMessage) {
        String[] dialogFlowResponse = dialogFlowMessage.getResolvedQuery().split(" ");
        String date = "";
        String city = "";
        if (dialogFlowMessage.getCityName() != null) {
            city = dialogFlowMessage.getCityName().replaceAll(" ", "%20");
            date = dialogFlowMessage.getDate();
        }

        response.add("email", email.getEmail(dialogFlowResponse));
        response.add("weather", climatempoService.getWeaterForecast(city, date));
        return response;
    }

    private ST cleanStringTemplateValue(ST response) {
        response.remove("email");
        response.remove("weather");
        return response;
    }

    private Map<String, ST> buildResponseMap() {
        responseMap = new HashMap<>();
        responseMap.put("saudacao", new ST("Oi, poderia me informar seu email?"));
        responseMap.put("nome", new ST("Tonhão Ranca Baço"));
        responseMap.put("idade", new ST("28"));
        responseMap.put("email", new ST("Seu email é: <email>"));
        responseMap.put("clima", new ST("<weather>"));
        responseMap.put("fallback", new ST("Justamente :D HAHA"));
        return responseMap;
    }
}