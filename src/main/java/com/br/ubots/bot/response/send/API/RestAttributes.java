package com.br.ubots.bot.response.send.API;

import org.springframework.http.*;
import java.nio.charset.Charset;

public class RestAttributes {

    public static HttpHeaders buildDialogFlowHttpHeader(String auth) {
        HttpHeaders header = buildHttpHeaders();
        header.set("Authorization", auth);
        return header;
    }

    public static HttpHeaders buildHttpHeaders() {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));
        return header;
    }

    public static HttpEntity<String> buildHttpEntity(String mensagem, HttpHeaders header) {
        return new HttpEntity<>(mensagem, header);
    }

}
