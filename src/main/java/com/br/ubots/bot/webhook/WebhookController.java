package com.br.ubots.bot.webhook;

import org.springframework.web.bind.annotation.*;

@RestController
public class WebhookController {

    @GetMapping("/webhook")
    public String RequestParam (@RequestParam(value="hub.challenge") String challenge) {
        return challenge;
    }
}
